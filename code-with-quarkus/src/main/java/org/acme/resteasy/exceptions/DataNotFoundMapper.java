package org.acme.resteasy.exceptions;

import org.acme.resteasy.Models.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundMapper implements ExceptionMapper<DataNotFound> {


    @Override
    public Response toResponse(DataNotFound exception) {
        ErrorMessage errorMessage=new ErrorMessage(exception.getMessage(), exception.statusCode, "");
        return Response.status(404).entity(errorMessage).build();
    }
}
