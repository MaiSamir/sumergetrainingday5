package org.acme.resteasy.Services;
import org.acme.resteasy.Models.Student;

import org.acme.resteasy.exceptions.DataNotFound;
import org.acme.resteasy.exceptions.WrongFormat;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;




class StudentServiceTest {
	StudentService studentService;


	@BeforeEach
	void init(){
		studentService=new StudentService();
	}

	@Test
	void testGetStudentsByInvalidYear() {
		assertAll(
				()-> assertTrue(studentService.getStudentsbyYear(0).isEmpty()),
				() ->assertTrue(studentService.getStudentsbyYear(-5).isEmpty()),
				() ->assertTrue(studentService.getStudentsbyYear(3000).isEmpty())
		);
	}


	@Test
	void testGetStudentByInvalidID() {

		assertThrows(DataNotFound.class,()->studentService.getStudentByID(-1));

	}

	@Test
	void testAddInvalidStudent() {
		Student student= new Student();
		student.setMajor("major");
		student.setEnrollmentYear(2019);
		assertThrows(WrongFormat.class,()->studentService.addStudent(student));
	}

	@Test
	void testUpdateNonExistingStudent() {
		Student student= new Student();
		student.setId(-100);
		student.setName("Name");
		assertThrows(DataNotFound.class,()->studentService.updateStudent(student));
	}




}
