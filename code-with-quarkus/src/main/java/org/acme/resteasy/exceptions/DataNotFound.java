package org.acme.resteasy.exceptions;

public class DataNotFound extends RuntimeException {


    int statusCode=404;

    public DataNotFound(String message) {
        super(message);
    }

    public int getStatusCode() {
        return statusCode;
    }
}
