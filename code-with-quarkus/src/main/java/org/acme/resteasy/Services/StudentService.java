package org.acme.resteasy.Services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.acme.resteasy.Models.Student;
import org.acme.resteasy.database.DatabaseClass;
import org.acme.resteasy.exceptions.DataNotFound;
import org.acme.resteasy.exceptions.WrongFormat;


public class StudentService{

    private static Map<Long,Student> students;

    public StudentService(){
        students= DatabaseClass.getStudents();
    }

    public static void setStudents(Map<Long, Student> students) {
        StudentService.students = students;
    }


    public List<Student> getAllStudents(){

        return new ArrayList<>(students.values());
    }

    public List<Student> getStudentsbyYear(int year){
        return students.values().stream()
                .filter(student -> student.getEnrollmentYear()==year)
                .collect(Collectors.toList());

    }
    public List<Student> getStudentsbyMajor(String major){
        return students.values().stream()
                .filter(student -> student.getMajor().equals(major))
                .collect(Collectors.toList());
    }
    public List<Student> getStudentsbyName(String name){
        List<Student> s= students.values().stream()
                .filter(student -> student.getName().equals(name))
                .collect(Collectors.toList());



        return s;
    }

    public List<Student> getStudentsPaginated(int start, int size){
        return new ArrayList<>(students.values()).subList(start,start+size);
    }

    public Student getStudentByID(long id){
        Student s=students.get(id);
        if(s==null){
            throw new DataNotFound("Student with id: "+id+" not found");
        }

        return s;
    }

    public Student addStudent(Student student){
        long newID= (students.size() + 1);
        while(students.containsKey(newID)) {
            newID+=1;}

        if(student.getName()==null){
            throw new WrongFormat("Student must have name field");
        }
        if(student.getMajor()==null){
            throw new WrongFormat("Student must have major field");
        }
        if(student.getEnrollmentYear()==0){
            throw new WrongFormat("Student must have enrollmentYear field");
        }

        student.setId(newID);

        students.put(student.getId(),student);
        return student;

    }
    public Student updateStudent(Student student){
        if(!students.containsKey(student.getId())) {
            throw new DataNotFound("Student with id: "+student.getId()+" not found");
        }
        if(student.getName()==null){
            throw new WrongFormat("Student must have name field");
        }
        if(student.getMajor()==null){
            throw new WrongFormat("Student must have major field");
        }
        if(student.getEnrollmentYear()==0){
            throw new WrongFormat("Student must have enrollmentYear field");
        }
        students.put(student.getId(),student);
        return student;


    }

    public Student deleteStudent(Long id){
        if(!students.containsKey(id)) {
            throw new DataNotFound("Student with id: "+id+" not found");
        }
        return students.remove(id);
    }




}