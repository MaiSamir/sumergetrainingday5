package org.acme.resteasy.exceptions;

public class WrongFormat extends RuntimeException {
    int statusCode=400;
    public WrongFormat(String message) {
        super(message);
    }

    public int getStatusCode() {
        return statusCode;
    }
}
