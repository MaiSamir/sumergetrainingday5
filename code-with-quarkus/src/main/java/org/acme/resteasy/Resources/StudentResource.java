package org.acme.resteasy.Resources;


import org.acme.resteasy.Models.Student;
import org.acme.resteasy.Services.StudentService;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;


public class StudentResource implements StudentInt{


    StudentService studentService = new StudentService();

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public Response getStudents(int year,int start, int size,String major,String name) {
        //filter by year
        if(year>0){
            return (Response.ok(studentService.getStudentsbyYear(year)).build());
        }
        //paginate
        if(start>=0 && size>0){
            //start out of bound
            if(start> studentService.getAllStudents().size()){
                return(Response.status(404).entity("start greater than students count").build());
            }
            //size out of bound - return from start till end
            if(start+size> studentService.getAllStudents().size()){
                return(Response.ok(studentService.getStudentsPaginated(start, studentService.getAllStudents().size()-start)).build());
            }
            //return requested page
            return(Response.ok(studentService.getStudentsPaginated(start,size)).build());
        }
        //filter by major
        if(major!=null){

            return(Response.ok(studentService.getStudentsbyMajor(major)).build());
        }
        //filter by name
        if(name!=null) {
            return (Response.ok(studentService.getStudentsbyName(name)).build());
        }
        //return all students unfiltered
        return(Response.ok(studentService.getAllStudents()).build());
    }

    @Override
    public Response getStudentByID(Long id) {
        Student student= studentService.getStudentByID(id);
        return Response.ok(student).build();
    }

    @Override
    public Response addStudent(Student student, UriInfo uriInfo) {
        //Successful add
        Student newStudent= studentService.addStudent(student);
        URI uri= uriInfo.getAbsolutePathBuilder().path(String.valueOf(student.getId())).build();
        return(Response.created(uri)
                .entity(newStudent)
                .build());
    }

    @Override
    public Response updateStudent(Long id, Student student) {
        student.setId(id);
        //Successful update
        Student updatedStudent= studentService.updateStudent(student);
        return Response.ok(updatedStudent).build();

    }

    @Override
    public Response deleteStudent(Long id) {
        Student deletedStudent= studentService.deleteStudent(id);
        //Successful delete
        return (Response.ok(deletedStudent).build());
    }


}