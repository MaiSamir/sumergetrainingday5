package org.acme.resteasy.Models;

public class Student{
    private long id;
    private String name;
    private String major;
    private int enrollmentYear;

    public Student(){

    }
    public Student(long id, String name, String major, int year){
        this.id=id;
        this.enrollmentYear=year;
        this.name=name;
        this.major=major;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public int getEnrollmentYear() {
        return enrollmentYear;
    }

    public void setEnrollmentYear(int enrollmentYear) {
        this.enrollmentYear = enrollmentYear;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }



}