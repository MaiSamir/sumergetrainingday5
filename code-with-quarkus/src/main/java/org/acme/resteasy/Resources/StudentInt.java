package org.acme.resteasy.Resources;
import org.acme.resteasy.Models.Student;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Path("/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface StudentInt {

    @GET
    Response getStudents(
             @QueryParam("year") int year
            ,@QueryParam("start") int start
            ,@QueryParam("size") int size
            ,@QueryParam("major") String major
            ,@QueryParam("name") String name);

    @Path("/{studentId}")
    @GET
    Response getStudentByID(@PathParam("studentId") Long id);

    @POST
    Response addStudent(Student student, @Context UriInfo uriInfo);

    @Path("/{studentId}")
    @PUT
    Response updateStudent(@PathParam("studentId") Long id, Student student);

    @Path("/{studentId}")
    @DELETE
    Response deleteStudent(@PathParam("studentId") Long id);



}
