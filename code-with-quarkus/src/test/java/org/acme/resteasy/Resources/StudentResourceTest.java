package org.acme.resteasy.Resources;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.acme.resteasy.Models.ErrorMessage;
import org.acme.resteasy.Models.Student;
import org.acme.resteasy.Services.StudentService;
import org.acme.resteasy.exceptions.DataNotFound;
import org.acme.resteasy.exceptions.DataNotFoundMapper;
import org.acme.resteasy.exceptions.WrongFormat;
import org.junit.jupiter.api.BeforeEach;

import org.junit.jupiter.api.Test;
import org.mockito.Spy;


import javax.ws.rs.core.Response;
import java.util.ArrayList;



class StudentResourceTest {
	StudentResource studentResource;
	@Spy
	StudentService studentServiceSpy;


	@BeforeEach
	void init(){
		studentResource=new StudentResource();
		DataNotFoundMapper mapper= new DataNotFoundMapper();
	}


	@Test
	void testCallServiceGetByYear() {
		StudentService mockStudentService=mock(StudentService.class);
		studentResource.setStudentService(mockStudentService);
		studentResource.getStudents(2020,0,0,null,null);
		verify(mockStudentService,times(1)).getStudentsbyYear(2020);

	}
	@Test
	void testGetStudentsByCorrectName() {
		StudentService mockStudentService=mock(StudentService.class);
		studentResource.setStudentService(mockStudentService);
		Student s= new Student();
		s.setName("Name");
		ArrayList<Student> mockStudents=new ArrayList<>();
		mockStudents.add(s);
		when(mockStudentService.getStudentsbyName("Name")).
				thenReturn(mockStudents);
		Response response= studentResource.getStudents(0,0,0,null,"Name");
		assertAll(
				()->assertEquals(mockStudents,response.getEntity()),
				()-> assertEquals(200,response.getStatus())

		);

	}

	@Test
	void testGetStudentsByCorrectMajor() {
		studentServiceSpy=spy(new StudentService());
		studentResource.setStudentService(studentServiceSpy);
		Student s= new Student();
		s.setMajor("Major");
		ArrayList<Student> mockStudents=new ArrayList<>();
		mockStudents.add(s);
		doReturn(mockStudents).when(studentServiceSpy).getStudentsbyMajor("Major");
		Response response= studentResource.getStudents(0,0,0,"Major",null);
		assertAll(
				()->assertEquals(mockStudents,response.getEntity()),
				()-> assertEquals(200,response.getStatus())

		);

	}

	@Test
	void testGetStudentByValidID() {
		StudentService mockStudentService=mock(StudentService.class);
		studentResource.setStudentService(mockStudentService);
		when(mockStudentService.getStudentByID(17)).thenReturn(new Student());
		Response response=studentResource.getStudentByID((long) 17);
		assertEquals(200,response.getStatus());

	}
	@Test
	void testGetStudentByInValidID() {

		Response response=null;

		try {

			response = studentResource.getStudentByID((long) -1);

		}
		catch(DataNotFound exception)
		{

			assertEquals(404, exception.getStatusCode());
		}

	}


	@Test
	void testAddStudentWithNoName() {

			Student student= new Student();
			student.setMajor("major");
			student.setEnrollmentYear(2020);
			try {
				studentResource.addStudent(student, null);

			}
			catch (WrongFormat exception){
					assertEquals(400,exception.getStatusCode());
			}


	}
	@Test
	void testAddStudentWithNoAttributes() {

		Student student= new Student();
		try {
			studentResource.addStudent(student, null);

		}
		catch (WrongFormat exception){
			assertEquals(400,exception.getStatusCode());
		}

	}

	@Test
	void testUpdateNonExistingStudent() {
		try{

			Response response= studentResource.deleteStudent((long)-1);

		}
		catch(DataNotFound exception){
			assertEquals(404,exception.getStatusCode());

		}

	}

	@Test
	void testDeleteNonExistingStudent() {

			try {
			studentResource.deleteStudent((long) -1);

			}
			catch (DataNotFound exception){
					assertEquals(404,exception.getStatusCode());
			}

	}

}
