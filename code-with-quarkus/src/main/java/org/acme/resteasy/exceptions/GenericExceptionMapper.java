package org.acme.resteasy.exceptions;

import org.acme.resteasy.Models.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class GenericExceptionMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable exception) {
        ErrorMessage errorMessage=new ErrorMessage(exception.getMessage(),500,"");
        return Response.status(500).entity(errorMessage).build();
    }
}
